package com.example.zeeshanjaved.drivetestingapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import static com.example.zeeshanjaved.drivetestingapp.MainActivity.getDriveResourceClient;

public class PhotoCaptureActivity extends AppCompatActivity {

    private static final int REQIEST_CODE_PICK_IMAGE = 1;
    private Button button, button2;
    private ImageView imageView;
    private Bitmap mBitmapToSave;
    private String uIdentifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_capture);

        button = (Button) findViewById(R.id.button_2);
        button2 = (Button) findViewById(R.id.button_3);
        imageView = (ImageView) findViewById(R.id.image);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), REQIEST_CODE_PICK_IMAGE);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mBitmapToSave !=null){
                    createFile();
                }
                else
                    Toast.makeText(PhotoCaptureActivity.this, "Select Image First!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createFile(){

        Log.e("Method Calling: ", "createFileCall_");
        uIdentifier = UUID.randomUUID().toString();
        final Bitmap image =mBitmapToSave;

        final Task<DriveFolder> rootFolderTask = getDriveResourceClient().getRootFolder();

        //Log.e("ROOT PATH", getDriveResourceClient().getRootFolder().toString());
        final Task<DriveContents> createContentsTask = getDriveResourceClient().createContents();
        Tasks.whenAll(rootFolderTask, createContentsTask)
                .continueWithTask(new Continuation<Void, Task<DriveFile>>() {
                    @Override
                    public Task<DriveFile> then(@NonNull Task<Void> task) throws Exception {

                        DriveFolder parent = rootFolderTask.getResult();
//                        driveid = parent.getDriveId();
//
//                        Log.e("ROOT FOLDER ID", driveid.toString());
                        DriveContents contents = createContentsTask.getResult();

                        OutputStream outputStream = contents.getOutputStream();
                        // Write the bitmap data from it.
                        ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();
                        // image.compress(Bitmap.CompressFormat.PNG, 100, bitmapStream);
                        image.compress(Bitmap.CompressFormat.JPEG,100, bitmapStream);

                        try {
                            outputStream.write(bitmapStream.toByteArray());

                        }
                        catch (IOException e){
                            Log.e("createFileIntentSen:", "Unable to write file contents.", e);
                            Toast.makeText(getApplicationContext(),"Unable to write file contents", Toast.LENGTH_LONG).show();
                        }

                        MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                .setTitle(uIdentifier.concat(".jpg"))
                                .setMimeType("image/jpeg")
                                .setStarred(true)
                                .build();

                        return getDriveResourceClient().createFile(parent, changeSet, contents);
                    }
                })
                .addOnSuccessListener(this,
                        new OnSuccessListener<DriveFile>() {
                            @Override
                            public void onSuccess(DriveFile driveFile) {
//                                showMessage(getString(R.string.file_created,
//                                        driveFile.getDriveId().encodeToString()));
                                Toast.makeText(PhotoCaptureActivity.this, "File uploaded!",Toast.LENGTH_SHORT).show();
//                                try {
//                                    saveIntoSqlite();
//                                }catch (Exception e){
//                                    Log.e("SaveintoSqlite: ", e.toString());
//                                }

                                //  finish();
                            }
                        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Zeeshan ", "Unable to create file", e);
                      //  showMessage(getString(R.string.file_create_error));
                        Toast.makeText(PhotoCaptureActivity.this, "Unable to create file", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){

            case REQIEST_CODE_PICK_IMAGE:
                Log.e("Request code pick image", Integer.toString(requestCode));

                if(resultCode == RESULT_OK){

                    Log.e("Result code capt image", Integer.toString(resultCode));
                    Toast.makeText(this, "Image Captured successfully.", Toast.LENGTH_SHORT).show();
                    try {
                        mBitmapToSave = (Bitmap) data.getExtras().get("data");
                        imageView.setImageBitmap(mBitmapToSave);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            default:
                Toast.makeText(getApplicationContext(),"Wrong Input",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
