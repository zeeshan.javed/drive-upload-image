package com.example.zeeshanjaved.drivetestingapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveResourceClient;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_SIGN_IN =1 ;
    private TextView textView;
    protected static GoogleSignInClient mGoogleSignInClient;
    protected static DriveClient mDriveClient;
    protected static DriveResourceClient mDriveResourceClient;
    private GoogleSignInAccount account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textviewgdrive);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SignIn();
            }
        });
    }

    private void SignIn(){
        Log.e("SignIn_Method", "Start sign in");
        mGoogleSignInClient = buildGoogleSignInClient();

        startActivityForResult(mGoogleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }


    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();

        return GoogleSignIn.getClient(this, signInOptions);
    }

    protected static DriveResourceClient getDriveResourceClient() {
        return mDriveResourceClient;
    }
    protected static DriveClient getDriveClient() {
        return mDriveClient;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REQUEST_CODE_SIGN_IN:
                if (resultCode == RESULT_OK) {
                    Log.e("Request Code Sign in", Integer.toString(requestCode));

                    if (resultCode == RESULT_OK) {
                        Log.e("Result Code Sign in", Integer.toString(resultCode));
                        Log.e("Result Code Sign in", "Sign In Successfully");
                        Toast.makeText(this, "Signed in successfully.", Toast.LENGTH_LONG).show();

                        // Use the last signed in account here since it already have a Drive scope.
//                        mDriveClient = Drive.getDriveClient(this, GoogleSignIn.getLastSignedInAccount(this));
//                        // Build a drive resource client.
//                        mDriveResourceClient =
//                                Drive.getDriveResourceClient(this, GoogleSignIn.getLastSignedInAccount(this));
                        account = GoogleSignIn.getLastSignedInAccount(this);
                        if(account !=null){
                            mDriveClient = Drive.getDriveClient(this, GoogleSignIn.getLastSignedInAccount(this));
                            mDriveResourceClient =
                                    Drive.getDriveResourceClient(this, GoogleSignIn.getLastSignedInAccount(this));
//                            personEmail = account.getEmail();
                        }
                        Intent intent = new Intent(this, PhotoCaptureActivity.class);
//                        sharedPreferences = getSharedPreferences("email", Context.MODE_APPEND);
//                        editor = sharedPreferences.edit();
//                        editor.putString("EMAIL", personEmail).commit();
//                        intent.putExtra("EMAIL", personEmail);
                        startActivity(intent);
                    }
                }
                break;
            default:
                Toast.makeText(this, "Error Occureed", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
